import fs from 'fs';
import ParcelService from './Parcel';
import { generateTrackingId } from './Tracking';

export function getOrders() {
  const ordersFile = fs.readFileSync('./wing-test-data/orders.json', { encoding: 'utf8' });
  const parsedOrders = JSON.parse(ordersFile);
  return parsedOrders.orders;
}

export function getCatalog() {
  const itemsFile = fs.readFileSync('./wing-test-data/items.json', { encoding: 'utf8' });
  const { items } = JSON.parse(itemsFile);
  const catalog = {};
  items.forEach((item, index) => {
    catalog[item.id] = items[index];
  });
  return catalog;
}

function sortItems(items, catalog) {
  const sortedItems = items.sort((a, b) => {
    const aReferenceItem = catalog[a.item_id];
    const bReference = catalog[b.item_id];
    return bReference.weight - aReferenceItem.weight;
  });
  const flatSortedItems = [];
  sortedItems.forEach(item => {
    for (let index = 0; index < item.quantity; index++) {
      flatSortedItems.push({ item_id: item.item_id });
    }
  });
  return flatSortedItems;
}

export async function handleOrders(orders, catalog) {
  let parcels = [];
  const parselService = new ParcelService();
  orders.forEach(order => {
    const sortedItems = sortItems(order.items, catalog);
    const parcelsBuilt = parselService.buildParcels(order.id, sortedItems, catalog);
    parcels = parcels.concat(parcelsBuilt);
    return parcelsBuilt;
  });
  parcels = await Promise.map(parcels, async parcel => {
    const trackingId = await generateTrackingId();
    parcel.tracking_id = trackingId;
    return parcel;
  });

  return parcels;
}