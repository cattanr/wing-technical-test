const MAX_WEIGHT = 30;
export default class SolverService {

  static gloutonResolve(items, catalog) {
    const parcelItems = [];
    let parcelWeight = 0;
    items.forEach((item, index) => {
      const itemWeight = parseFloat(catalog[item.item_id].weight);
      const weightToTry = parseFloat(parcelWeight) + parseFloat(itemWeight);
      if (weightToTry <= MAX_WEIGHT) {
        parcelItems.push(item);
        parcelWeight = weightToTry.toFixed(1);
        items.splice(index, 1);
      }
    });
    return { parcelItems, parcelWeight };
  }

}