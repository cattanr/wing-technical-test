import * as OrderService from './Order';
import * as ParcelService from './Parcel';
import * as SolverService from './Solver';
import * as ShipmentService from './Shipment';
import * as TrackingService from './Tracking';

export {
  OrderService,
  ParcelService,
  SolverService,
  ShipmentService,
  TrackingService,
};