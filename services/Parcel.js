import SolverService from './Solver';

export default class ParcelService {

  constructor() {
    this.PARCEL_PER_PALLET = 15;
    this.PARCEL_COUNT = 0;
    this.PALLET_NUMBER = 1;
  }

  buildParcels(orderId, items, catalog) {
    let itemProceedNumber = 0;
    const parcels = [];
    while (itemProceedNumber < items.length) {
      const { parcelItems, parcelWeight } = SolverService.gloutonResolve(items, catalog);
      this.PARCEL_COUNT++;
      if (this.PARCEL_COUNT % this.PARCEL_PER_PALLET === 0) this.PALLET_NUMBER++;
      const parcel = {
        order_id: orderId,
        items: parcelItems,
        weight: parcelWeight,
        palette_number: this.PALLET_NUMBER,
      };
      itemProceedNumber += parcelItems.length;
      parcels.push(parcel);
    }
    return parcels;
  }

}