export function calculatePayment(parcels) {
  return parcels.reduce((sum, current) => {
    let weightValue = 0;
    const weight = parseFloat(current.weight);
    if (weight > 0 && weight <= 1) weightValue = 1;
    if (weight > 1 && weight <= 5) weightValue = 2;
    if (weight > 5 && weight <= 10) weightValue = 3;
    if (weight > 10 && weight <= 20) weightValue = 5;
    if (weight > 20) weightValue = 10;
    return sum + weightValue;
  }, 0);
}