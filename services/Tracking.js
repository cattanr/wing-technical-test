import got from 'got';

export async function generateTrackingId() {
  try {
    const options = {
      url: 'http://www.randomnumberapi.com/api/v1.0/randomstring?min=15&max=15&count=1',
      method: 'GET',
    };
    const response = await got(options);
    return response?.body ? JSON.parse(response.body)[0] : undefined;
  } catch (error) {
    console.log(error);
  }
}