import Promise from 'bluebird';
import { OrderService, ShipmentService } from './services';

global.Promise = Promise;

async function start() {
  const orders = OrderService.getOrders();
  const catalog = OrderService.getCatalog();
  const parcels = await OrderService.handleOrders(orders, catalog);
  const shipmentPayment = ShipmentService.calculatePayment(parcels);
  return { parcels, shipmentPayment };
}

start().then(shipmentDetails => {
  // Uncomment for console debug
  // const { parcels, shipmentPayment } = shipmentDetails;
  // console.log(parcels, shipmentPayment);
  return shipmentDetails;
});
